﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;

namespace QuizDosCamila.Droid
{
    [Activity(Label = "QuizDosCamila", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            string name_file_db = "miscomidas.sqlite";
            // ruta en android
            //bucar los archivos personales de donde esta la base de datos
            //folder o carpeta donde se guarda la bd
            string path_android = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            //Ruta exacta donde se encuntra la base de datos
            string path_db = Path.Combine(path_android, name_file_db);

            LoadApplication(new App(path_db));

        }
    }
}

