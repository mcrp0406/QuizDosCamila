﻿using QuizDosCamila.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QuizDosCamila
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}



        public void Lis_Com(object sender, EventArgs e)
        {

            Comidas comida = new Comidas()
            {
                Name_Comida = entryName.Text,
            };


            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                connection.CreateTable<Comidas>();
                var result = connection.Insert(comida);

                if (result > 0)
                {
                    liscomi();
                    DisplayAlert("Correcto", "Guradado", "Ok");
                    entryName.Text = "";

                }
                else
                {
                    
                    DisplayAlert("Error", "No Se Guardo", "Ok");
                }
            };
        }


        public void liscomi() { 
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                List<Comidas> listaComidas;
                listaComidas = connection.Table<Comidas>().ToList();
                listViewComida.ItemsSource = listaComidas;
            }
        }
    }

}

