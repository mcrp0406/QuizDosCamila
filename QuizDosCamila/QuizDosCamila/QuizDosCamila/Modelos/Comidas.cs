﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizDosCamila.Modelos
{
    class Comidas
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string Name_Comida { get; set; }

    }
}
